function dibuixarTaula(){
    //Esborro la taula anterior en cas que s'hagi creat
    document.getElementById("table").innerHTML = '';

    //Variables fila - columna
    var files = document.getElementById('files').value;
    var columnes = document.getElementById('columnes').value;
    var colors = ["red", "orange", "yellow"];   //Array amb els 3 colors
    var contador = 0;   //Amb aquest contador s'anira iterant entre els colors

    for (var i = 0; i < files; ++i) {   //ITERACIO FILES

        //Es crea una fila
        var tr = document.createElement('tr');
        
        for (var c = 0; c < columnes; ++c) {   //ITERACIO COLUMNES

            //es crea la columna
            var td = document.createElement('td');

            //Faig que la columna sigui child de fila
            tr.appendChild(td);
            
            //Li poso el color de fons 
            td.style.backgroundColor = colors[contador];
            contador++;

            if(contador > 2) {  //Si és l'ultim color de l'Array, torno al primer
                contador = 0;
            }

        }
        //Faig que la fila sigui child de taula
        table.appendChild(tr);
    }
}
