function OperacioPotencia(num1, num2) { // Funcio per fer la potencia
    if(num2 == 0){
       return 1
    }
    else if (num1 == 0){
        return 0;
    }
    else {
        return num1 * OperacioPotencia(num1, num2-1);
    }
}

function OperacioFactorial(num){    //Funcio per fer el factorial
    if (num < 0) {
        return -1;
    }
    else if (num == 0) {
        return 1;
    }
    else {
        return (num * OperacioFactorial(num - 1));
    }
}

function OperacioData(cad){ //Funcio per fer la Data
    var split = cad.split("/");
    var dia = split[0];
    var mes = split[1];
    var any = split[2];
    var data = new Date(any,mes-1,dia);
    data.setDate(data.getDate()+1);
    data.toLocaleDateString();
    return data;
}

function calculsBasics() {  //Selector desplegable
    var n1 = parseFloat(document.getElementById('n1').value);
    var n2 = parseFloat(document.getElementById('n2').value);
    var oper = document.getElementById('operadors').value;

    if(oper === '+') {
        document.getElementById('result').value = n1+n2;
    }
    
    if(oper === '-') {
        document.getElementById('result').value = n1-n2;
    }
    
    if(oper === '/') {
        document.getElementById('result').value = n1/n2;
    }
    
    if(oper === 'X') {
        document.getElementById('result').value = n1*n2;
    }
}

function Potencia(){
    var n1 = document.getElementById("n1").value;
    var n2 = document.getElementById("n2").value;
    var resultat=OperacioPotencia(parseInt(n1),parseInt(n2));
    document.getElementById('result').value = resultat;
}

function Concatenar(){
    var n1 = document.getElementById("n1").value;
    var n2 = document.getElementById("n2").value;
    var concat = n1 + n2;
    document.getElementById('result').value = concat;
}

function Ordenar(){
    var array=[1,2,3,4,5,6,7,8,9,10];
    for (x=0;x<array.length;x++) {
        var numero = prompt("Introdueix el numero", x+1);
        array[x] = numero;
    }
    array.sort(function(a, b){return a - b});
    array.toString();
    document.getElementById('result').value = array;

}

function Factorial(){
    var n1 = document.getElementById("n1").value;
    var n2 = OperacioFactorial(parseFloat(n1));
    if (n1 < 0){
        document.getElementById('result').value = ("Error: numero negatiu.");
    }
    else{
        document.getElementById('result').value = (n2);
    }
}

function Data(){
    var n1 = document.getElementById("n1").value;
    var data=OperacioData(n1);
    document.getElementById('result').value = data;
}

function ArrelQuadrada() {
    var n1 = document.getElementById("n1").value;
    var resultat = Math.sqrt(parseFloat(n1));
    document.getElementById('result').value = resultat;
}

function EliminarNegatius() {
    var string = document.getElementById("n1").value;
    var split = string.split(" ");
    if (split.length > 10){
        document.getElementById('result').value = "Error: màxim nombre de numeros permes: 10";
    }
    else {
        split = split.filter(function(x){ return x > -1 });
        document.getElementById('result').value = (split);
    }
}

function GenerarAleatori(){
    var n1 = document.getElementById("n1").value;
    var n2 = document.getElementById("n2").value;

    var resultat = Math.floor(Math.random()*(Math.max(n1, n2)-Math.min(n1, n2)+1)) + Math.min(n1, n2);

    document.getElementById('result').value = resultat;
}

/** Hola Joan,
He intentat fer:
	var n1 = document.getElementById("n1").value;
    var n2 = document.getElementById("n2").value;

    i llavors
    if(isNaN(n1)){} <-- em retorna true, per tant es compleix l'if
    	document.getElementById('result').value = "No es un numero valid!";
    }

Pero resulta que per alguna raó no funciona i aixi no puc cubrir un dels errors produits per
l'usuari (el de introduir una lletra enves d'un numero, per exemple.)
 */